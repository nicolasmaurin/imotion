**Ce travail a été réalisé par MAURIN Nicolas & MAULON Thomas lors du cours "Architecture générale et logicielle des systèmes embarqués"**

# Présentation :

Le but de l'atelier était de se mettre dans la tête d'un robot parcourant une pièce de maison.

Les murs et obstacles sont définis par un "-1", les espaces non scanés sont définis par un 0.

Le robot débute sur la case 1 du fichier `tableau_imotion.xlsx`.

# Methode :

Le fichier `Scan.php` contient toute la logique métier. 

On remarquera plusieurs méthodes commentées qui ont permis d'implémenter au fur et à mesure l'algo.

La class Format contient les méthodes pour lire un fichier XLSX et sortir le résultat de Scan dans un fichier CSV.

Le fichier `index.php` assemble notre logique et propose la solution de l'atelier.

# Fonctionnement :

Lancer le Docker contenant le server et PHP 8.0 avec la commande :

`docker-compose up --build`

Ouvrez votre navigateur et ouvrez la page `localhost` ce qui lancera les scripts situés dans `index.php`.

Le fichier `tableau_imotion.xlsx` est le fichier d'entrée décrivant la pièce à parcourir. 
Ce fichier peut être modifier pour changer l'organisaton de la pièce.

Une fois que le robot a parcouru la pièce, le fichier `result.csv` sera mis à jour (facilement lu par un IDE).

Exemple résultat avec un jeux de données `data_test` dans `index.php` :

![Déroulement du script](./assets/Capture_decran_2021-02-10_a_08.41.37.png)

![Tableau final](./assets/Capture_decran_2021-02-10_a_08.41.59.png)