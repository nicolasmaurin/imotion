<?php

use Main\Format;
require_once 'Format.php';
require("./scan.php");

$test = new Format();
$file = $test->readExcelFile('tableau_imotion.xlsx');

$data_test = array(
    array(-1, -1, -1, -1, -1),// array[0]
    array(-1, 1, 0, 0, -1),
    array(-1, 0, 0, 0, -1),
    array(-1, 0, -1, 0, -1),
    array(-1, 0, 0, 0, -1),
    array(-1, 0, 0, 0, -1),
    array(-1, -1, -1, -1, -1),
);

$scannedGrid = scan($file);

$arrayToCSV = $test->exportIntoCSVXFile($scannedGrid, 'result.csv');