<?php

/**
 * 1er cas : On parse ligne après ligne et colonne après colonne sans contrainte
 * 2eme cas : On parse ligne après ligne et colonne après colonne en allant de gauche vers la droite puis de droite vers la gauche quand on arrive au bout et toujours de haut en bas 
 * 3eme cas : On prend le même sens de direction que le cas 2. Puis, Si on trouve un obstacle sur une colonne de la ligne en cours, alors on change de ligne et on reprend le parcours au début de la ligne suivante.
 * 4eme cas KO : On prend le même sens de direction que le cas 2. Puis, Si on trouve un obstacle sur une colonne de la ligne en cours, alors on change de ligne et on reprend le parcours avec la direction précédente et à partir de la colonne avant l'obstacle.
 * 5eme cas TODO : On prend le même sens de direction que le cas 2. Puis, Si on trouve un obstacle sur une colonne de la ligne en cours, alors on change de ligne jusquà qu'il n'y ait plus d'obstacle sur la ligne précédente. 
 *            Sinon, on repasse sur la ligne précédente et on reprend le parcours avec la direction précédente et à partir de la colonne où il n'y a plus d'obstacle.
 */

// Implem 1er cas OK
/*
function scan($lines) {
    $initialXBotPosition = 1;
    $initialYBotPosition = 1;
    $botPosition = $lines[$initialXBotPosition][$initialYBotPosition];
    echo "Number at initial Bot position = lines[$initialXBotPosition][$initialYBotPosition] = $botPosition \n";
    $isLeftToRightDirection = true;
    $lastRowIndex = count($lines) - 1; 
    echo "Last row index is $lastRowIndex \n";
    for ($row = 1; $row <= $lastRowIndex; $row++) { 
        $lastColIndex = count($lines[$row]) - 1; 
        echo "Last col index is $lastColIndex \n";
        if($isLeftToRightDirection) {
            for ($col = 0; $col <= $lastColIndex; $col++){
                $currentNumber = $lines[$row][$col];
                if ($currentNumber !== -1) {
                    echo "Number at current position of lines[$row][$col] = $currentNumber \n";
                    $lines[$row][$col] = 1;
                }
            }
        }
    }

    echo "new array of lines = " . print_r($lines);
}
*/
// Implem 2ème cas OK
/*
function scan($lines) {
    $initialXBotPosition = 1;
    $initialYBotPosition = 1;
    $botPosition = $lines[$initialXBotPosition][$initialYBotPosition];
    echo "Number at initial Bot position = lines[$initialXBotPosition][$initialYBotPosition] = $botPosition \n";
    $isLeftToRightDirection = true;
    $lastRowIndex = count($lines) - 1; 
    for ($row = 1; $row <= $lastRowIndex; $row++) { 
        $lastColIndex = count($lines[$row]) - 1; 
        if($isLeftToRightDirection) {
            for ($col = 0; $col <= $lastColIndex; $col++){
                $currentNumber = $lines[$row][$col];
                // $numberAtTheNextPosition = $lines[$row][$col + 1];
                if ($currentNumber !== -1) {
                    echo "Number at current position of lines[$row][$col] = $currentNumber \n";
                    $lines[$row][$col] = 1;
                }
            }
            $isLeftToRightDirection  = !$isLeftToRightDirection;
        } else {
            for ($col = $lastColIndex; $col >= 0; $col--){
                $currentNumber = $lines[$row][$col];
                // $numberAtTheNextPosition = $lines[$row][$col + 1];
                if ($currentNumber !== -1) {
                    echo "Number at current position of lines[$row][$col] = $currentNumber \n";
                    $lines[$row][$col] = 1;
                }
            }
            $isLeftToRightDirection  = !$isLeftToRightDirection;
        }
    }

    echo "new array of lines = " . print_r($lines);
}
*/
// Implem 3ème cas OK
/*
function scan($lines) {
    $initialXBotPosition = 1;
    $initialYBotPosition = 1;
    $botPosition = $lines[$initialXBotPosition][$initialYBotPosition];
    echo "Number at initial Bot position = lines[$initialXBotPosition][$initialYBotPosition] = $botPosition \n";

    $isLeftToRightDirection = true;
    
    $findAnObstacle = false;
    
    $beginCircuit = true;
    $circuitEnded = false;
    
    $lastRowIndex = count($lines) - 1; 
    
    for ($row = $initialXBotPosition; ($row < $lastRowIndex) && (!$circuitEnded); $row++) { 
        $lastColIndex = count($lines[$row]) - 1;
        $nextRowIndex = $row + 1;
        if($isLeftToRightDirection) {
            for ($col = ($beginCircuit ? $initialYBotPosition : 0); ($col <= $lastColIndex) && ($findAnObstacle === false); $col++){
                $beginCircuit = false;
                $currentNumber = $lines[$row][$col];
                $nextColIndex = $col + 1;
                $numberAtTheNextPosition = $lines[$row][$nextColIndex];

                if (($nextRowIndex === $lastRowIndex) && ($nextColIndex === $lastColIndex)) {
                    echo "You can't go any further in the circuit.\n";
                    $circuitEnded = true;
                } else if(!$circuitEnded) {
                    echo "Number at the next position of lines[$row][$nextColIndex] = $numberAtTheNextPosition \n";
                    if ($numberAtTheNextPosition === -1) {
                        echo "So I pass the flag to false.\n";
                        $findAnObstacle = true;
                    } else if ($numberAtTheNextPosition === 0) {
                        echo "So I pass it to 1 and move.\n";
                        $lines[$row][$nextColIndex] = 1;
                    } else {
                        echo "NextNumber is already 1 so I can continue\n";
                    }
                }
            }
            $isLeftToRightDirection  = !$isLeftToRightDirection;
        } else {
            for ($col = $lastColIndex; ($col >= 0) && ($findAnObstacle === false); $col--){
                $currentNumber = $lines[$row][$col];
                $nextColIndex = $col - 1;
                $numberAtTheNextPosition = $lines[$row][$nextColIndex];

                if (($nextRowIndex === $lastRowIndex) && ($nextColIndex === $lastColIndex)) {
                    echo "You can't go any further in the circuit.\n";
                } else {
                    echo "Number at the next position of lines[$row][$nextColIndex] = $numberAtTheNextPosition \n";
                    if ($numberAtTheNextPosition === -1) {
                        echo "So I pass the flag to false\n";
                        $findAnObstacle = true;
                    } else if ($numberAtTheNextPosition === 0) {
                        echo "So I pass it to 1 and move.\n";
                        $lines[$row][$nextColIndex] = 1;
                    } else {
                        echo "NextNumber is already 1 so I can continue and move.\n";
                    }
                }
            }
            $isLeftToRightDirection  = !$isLeftToRightDirection;
        }

        if (!$circuitEnded) {
            echo "FindObstacle is = $findAnObstacle \n";
            $findAnObstacle = false;
            echo "Now i found an obstacle, I change of row and reinitialize findObstacle flag to change the line parsed.\n";
        }

    }
    echo "End of circuit. \n";
    echo "new array of lines = " . print_r($lines);
}
*/

// Implem 4ème cas Semi KO : On passe sur l'autre ligne et scanne la précédente pour y mettre un 1 si il n'y a pas d'obstacle

function scan($lines) {
    $initialXBotPosition = 1;
    $initialYBotPosition = 1;
    $botPosition = $lines[$initialXBotPosition][$initialYBotPosition];
    echo "Number at initial Bot position = lines[$initialXBotPosition][$initialYBotPosition] = $botPosition \n";
    
    $isLeftToRightDirection = true;
    
    $findAnObstacleInTheRow = false;
    $findTheEndOfRow = false;
    
    $beginCircuit = true;
    $circuitEnded = false;
    $lastRowIndex = count($lines) - 1;

    for ($row = $initialXBotPosition; ($row < $lastRowIndex) && (!$circuitEnded); $row++) { 
        $lastColIndex = count($lines[$row]) - 1;
        $nextRowIndex = $row + 1;
        if($isLeftToRightDirection) {
            for ($col = ($beginCircuit ? $initialYBotPosition : 0); (($col <= $lastColIndex) && ($findTheEndOfRow === false) && ($findAnObstacleInTheRow === false)) ; $col++){
                $beginCircuit = false;
                $currentNumber = $lines[$row][$col];
                $nextColIndex = $col + 1;
                $numberAtTheNextPosition = $lines[$row][$nextColIndex];

                if (($nextRowIndex === $lastRowIndex) && ($nextColIndex === $lastColIndex)) {
                    echo "You can't go any further in the circuit.\n";
                    $circuitEnded = true;
                } else if(!$circuitEnded) {
                    echo "Number at the next position of lines[$row][$nextColIndex] = $numberAtTheNextPosition \n";
                    if ($numberAtTheNextPosition === -1) {
                        echo "Next position have an obstacle.\n";
                        if ($nextColIndex === $lastColIndex) {
                            echo "In fact, I'm at the end of line. So I pass the findTheEndOfRow flag to true and I pass to next.\n";
                            $findTheEndOfRow = true;
                        } else if ($lines[$nextRowIndex][$col] !== -1) {
                            echo "I avoid the obstacle by passing on the next line on the lines[$nextRowIndex][$col] square if there is no obstacle.\n"; 
                            echo "lines[$nextRowIndex][$col] = " . $lines[$nextRowIndex][$col] . ". So, I pass it to 1 and move in.\n";
                            $lines[$nextRowIndex][$col] = 1;
        
                            $nextColIndexOnNextRow = $col + 1;
                            $numberAtTheNextPosition = $lines[$nextRowIndex][$nextColIndex];
                            
                            if(($lines[$row][$nextColIndex] === -1) && ($lines[$nextRowIndex][$nextColIndex] !== -1)) {
                                echo "Square at lines[$row][$nextColIndexOnNextRow] =" . $lines[$row][$nextColIndex] . " and square at lines[$nextRowIndex][$nextColIndex] = " . $lines[$nextRowIndex][$nextColIndex] . "\n";
                                echo "As number at the next position of lines[$nextRowIndex][$nextColIndex] = $numberAtTheNextPosition \n";
                                echo "And whereas the lines[$row][$nextColIndex] square in the previous line is equal to ". $lines[$row][$nextColIndex] .".\n";

                                if ($numberAtTheNextPosition === 0) {
                                    echo "I pass lines[$nextRowIndex][$nextColIndex] to 1 and move in.\n";
                                    $lines[$nextRowIndex][$nextColIndex] = 1;
                                } else {
                                    echo "NextNumber is already 1 so I can continue\n";
                                }
                            }
                        }                        
                    } else if($numberAtTheNextPosition === 0)  {
                        echo "So I pass it to 1 and move.\n";
                        $lines[$row][$nextColIndex] = 1;
                    } else {
                        echo "NextNumber is already 1 so I can continue\n";
                    }
                }
            }
            if($findTheEndOfRow) {
                $isLeftToRightDirection  = !$isLeftToRightDirection;
            }
        } else {
            for ($col = $lastColIndex; (($col >= 0) && ($findTheEndOfRow === false) && ($findAnObstacleInTheRow === false)) ; $col--){
                $currentNumber = $lines[$row][$col];
                $nextColIndex = $col - 1;
                $numberAtTheNextPosition = $lines[$row][$nextColIndex];

                if (($nextRowIndex === $lastRowIndex) && ($nextColIndex === 0)) {
                    echo "You can't go any further in the circuit.\n";
                    $circuitEnded = true;
                } else {
                    echo "Number at the next position of lines[$row][$nextColIndex] = $numberAtTheNextPosition \n";
                    if ($numberAtTheNextPosition === -1) {
                        echo "So I pass the flag to false\n";
                        if ($nextColIndex === 0) {
                            echo "I'm at the end of line, so I pass the findTheEndOfRow flag to true and I pass to next.\n";
                            $findTheEndOfRow = true;
                        } else {
                            $findAnObstacleInTheRow = true;
                        }

                    } else if ($numberAtTheNextPosition === 0) {
                        echo "So I pass it to 1 and move.\n";
                        $lines[$row][$nextColIndex] = 1;
                    } else {
                        echo "NextNumber is already 1 so I can continue and move.\n";
                    }
                }
            }
            if($findTheEndOfRow) {
                $isLeftToRightDirection  = !$isLeftToRightDirection;
            }
        }

        if (!$circuitEnded) {
            if($findTheEndOfRow) {
                echo "Now i found the end of row I change of row and reinitialize findTheEndOfRow flag to change the line parsed.\n";
                $findTheEndOfRow = false;
            }
        }
    }    
    echo "End of circuit. \n";
    echo "new array of lines = " . print_r($lines);
    return $lines;

}