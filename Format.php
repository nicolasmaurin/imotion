<?php


namespace Main;
use SimpleXLSX\SimpleXLSX;

require_once "./lib/SimpleXLSX.php";


class Format
{
    public function readExcelFile($file){
        $xlsx = new SimpleXLSX($file);
        $file = $xlsx::parse('tableau_imotion.xlsx');
        $dataInArray = $file->rows();

        return $dataInArray;

    }


    /**
     * @param $datas array
     * @param $filename string
     */
    public function exportIntoCSVXFile($datas, $filename) {
        $out = fopen($filename, 'w');

        foreach ($datas as $data)
        {
            fputcsv($out, $data,"\t");
        }
        fclose($out);
    }


}